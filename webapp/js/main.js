$(document).ready(function(){
    var select;
    $('#en').addClass('active');
    select=2;

    // removes welcome msg to reveal the main menu
    $("#welcomsg").click(function(){
        $(this).remove();
       
        //timer
        var timer2 = "00:40";
        var interval = setInterval(function() {
        
        
          var timer = timer2.split(':');
          //by parsing integer, to avoid all extra string processing
          var minutes = parseInt(timer[0], 10);
          var seconds = parseInt(timer[1], 10);
          --seconds;
          minutes = (seconds < 0) ? --minutes : minutes;
          if (minutes < 0) clearInterval(interval);
          seconds = (seconds < 0) ? 59 : seconds;
          seconds = (seconds < 10) ? '0' + seconds : seconds;
          //minutes = (minutes < 10) ?  minutes : minutes;
          $('.countdown').html(minutes + ':' + seconds);
          timer2 = minutes + ':' + seconds;
    
          if(seconds==00 && minutes==0){
            window.location.href = "https://i436649.hera.fhict.nl/smartmirror/";
          }

          $("body").click(function(){
             timer2 = 00 + ':' + 40;
          }); //end extra tijd 1

        }, 1000) //end timer
    
    });

    $( ".linktxt" ).click(function() {
        event.preventDefault();
        // clicking same btn <--
        // clears selected class from the clicked btn
        // replaces data section with the default msg
        if ($(this).hasClass('selected')){  
            $('.linktxt').removeClass('selected');
            $('#infoPanel').empty();
            $.ajax({ url: 'templates/model/directions.php', success: function(data){setTimeout(function(){
                $('#infoPanel').append(data);});     }});                         
        
        } 
        // clicking different btn <--
        // clears the selected class from previously clicked btn 
        // adds 'selected' class to a newly clicked btn
        // replaces data in info panel with the selected btn data
        else if ($('.linktxt').hasClass('selected')){  
            $('.linktxt').removeClass('selected');
            $(this).addClass('selected');
            $('#infoPanel').empty();
            $.ajax({ url: this, success: function(data){setTimeout(function(){
                $('#infoPanel').append(data);
                //slick slider thingy
                $('.slider').slick({
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    centerMode: true,
                    variableHeight: true
                });//slick ending slider thingy     
                
            });}});       
        }
        // first click from home pg <--
        // adds selected class to clicked btn and clears the middle panel of the previous info
        else{
        $(this).addClass('selected');
        $('#infoPanel').empty();

       // fills in the panel with current btn data
        $.ajax({ url: this, success: function(data){setTimeout(function(){
            $('#infoPanel').append(data);
            //slick slider thingy
            $('.slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                centerMode: true,
                variableHeight: true
            });//slick ending slider thingy
            
        });}});

        }
    }); //end click

    //clears main section / pannel 
    $( "#title" ).click(function() {
        $('.linktxt').removeClass('selected');
        $('#infoPanel').empty();
        $.ajax({ url: 'templates/model/directions.php', success: function(data){setTimeout(function(){
            $('#infoPanel').append(data);});    
        }});  

    });

        $(document.body).on( 'click',".lang-btn", function() {
           
            if($('#nl').hasClass('active')){
                $('#nl').removeClass('active');
                $('#en').addClass('active');
                $('#btnslider').removeClass('open');
                //translation
                $('#waterlink').attr("href", 'templates/model/water.php');
                $('#chemicalslink').attr("href", 'templates/model/chemicals.php');
                $('#cottonlink').attr("href", 'templates/model/cotton.php');
                $('#packagelink').attr("href", 'templates/model/packaging.php');
                $('#sociallink').attr("href", 'templates/model/social.php');
                $('#woollink').attr("href", 'templates/model/wool.php');
                $('#cotton-Lang-switch').text('Cotton');
                $('#water-Lang-switch').text('Water');
                $('#wool-Lang-switch').text('Wool');

                $('#social-Lang-switch').removeClass('active');
                $('#social-Lang-switch').text('Social Responsibility');
                
                $('#packaging-Lang-switch').text('Packaging');
                $('#chems-Lang-switch').text('Chemicals');

                // $('#infoPanel').empty();

                // $.ajax({ url: "templates/model/directions.php", success: function(data){setTimeout(function(){
                //     $('#infoPanel').empty();
                //     $('#infoPanel').append(data); });}});

                if($(".linktxt").hasClass('selected')){
                    var linkything = $(".linktxt.selected").attr('href');
                    console.log(linkything);
                    $('#infoPanel').empty();
                    console.log('if statement')
                    $.ajax({ url: linkything, success: function(data){setTimeout(function(){
                        $('#infoPanel').append(data);
                        //slick slider thingy
                        $('.slider').slick({
                            dots: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableHeight: true
                        });
                    })}})
                }

                select=2

            }else{
                
                $('#en').removeClass('active');
                $('#nl').addClass('active');
                $('#btnslider').addClass('open');
                //vertaling
                $('#waterlink').attr("href", 'templates/model/waterNL.php');
                $('#chemicalslink').attr("href", 'templates/model/chemicalsNL.php');
                $('#cottonlink').attr("href", 'templates/model/cottonNL.php');
                $('#packagelink').attr("href", 'templates/model/packagingNL.php');
                $('#sociallink').attr("href", 'templates/model/socialNL.php');
                $('#woollink').attr("href", 'templates/model/woolNL.php');

                $('#cotton-Lang-switch').text('Katoen');
                $('#water-Lang-switch').text('Water');
                $('#wool-Lang-switch').text('Wol');

                $('#social-Lang-switch').addClass('active');
                $('#social-Lang-switch').text('Sociale Verantwoordelijkheid');

                $('#packaging-Lang-switch').text('Verpakking');
                $('#chems-Lang-switch').text('Chemicaliën');

               

                // $('#infoPanel').empty();

                // // fills in the panel with current btn data
                //  $.ajax({ url: "templates/model/directionsNL.php", success: function(data){setTimeout(function(){
                //     $('#infoPanel').empty();
                //     $('#infoPanel').append(data); });}});

                if($(".linktxt").hasClass('selected')){
                    var linkything = $(".linktxt.selected").attr('href');
                    console.log(linkything);
                    $('#infoPanel').empty();
                    console.log('if statement')
                    $.ajax({ url: linkything, success: function(data){setTimeout(function(){
                        $('#infoPanel').append(data);
                        //slick slider thingy
                        $('.slider').slick({
                            dots: true,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1,
                            centerMode: true,
                            variableHeight: true
                        });
                    })}})
                }
                select=1           
            }
        });



        $(document).on('keypress',function(e) {
            if(e.which == 13) {
                console.log("enter has been pressed");
                window.location.href = "https://i436649.hera.fhict.nl/smartmirror/mirror.php";
            }
        });


        

}); //end document ready
