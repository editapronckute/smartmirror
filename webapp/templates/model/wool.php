<div id='infopack'>
    <div class="slideContainer">
        <div class="slider">
            <div><img class='anim-img' src="gif\sheep-transparent.gif" alt="wool">
            <p> Around 800,000 sheep need shearing at least once a year. 
                Each sheep provides <span class="highlight">4 kg of raw wool</span>, which could be used to knit two sweaters.
            </p></div>
            <div><img class='woolstats-img' src="img\woolstats.png" alt="wool"></div>
        </div>
    </div>
</div> <!-- end infopack -->