<div id='infopack'>
    <div class="slideContainer">
        <div class="slider">

        <div><img class='anim-img' src="gif\cottonani1.gif" alt="cotton">
        <p>In 2025 zijn we van plan om <span class="highlight">100%</span> te bereiken! Een kledingstuk zal maximaal <span class="highlight">20%</span> gerecycled en <span class="highlight">80%</span> biologisch katoen gebruiken voor ultieme duurzaamheid.</p></div>
            <div><img class='icon-logo' src="img\nogmo.png" alt="cotton">
                <p> We werken eraan om al het conventionele katoen in onze collecties te vervangen door <span class="highlight">biologische of 
                    gerecyclede katoenvezels</span>. Om katoen als biologisch te beschouwen,
                     moet het ten minste drie jaar worden verbouwd op land dat vrij is van pesticiden en meststoffen, <span class="highlight">zonder gebruik te maken van GGO's</span>.</p></div>
            <div><img class='bci-logo' src="img\bci-logo@2x.png" alt="cotton">
            <p>We ondersteunen het Better Cotton Initiative (BCI) als een betere oplossing waar technische beperkingen of beperkingen in de toeleveringsketen niet volledig aanwezig zijn om <span class="highlight">organische of gerecyclede</span> vezels in te kopen.
            </p></div>
        </div>
    </div> <!-- end slideContainer -->
</div> <!-- end infopack -->