<div id='infopack'>
    <div class="slideContainer">
        <div class="slider">
            <div><img class='anim-img' src="gif\chem-transparent.gif" alt="chemicals">
            <p id='chem1-text'> We have a list of over <span class="highlight">100 chemicals</span> (including chemical finishes 
            and procedures) that we <br> <span class="highlight">do not use </span> in the creation of our collections.</p></div>
            <div><img class='icon-logo' src="img\checklist.png" alt="chemicals">
            <p id='chem2-text'> <span class="highlight">All our suppliers</span> commit to and comply with the list, which ensures that the legislation surrounding the 
            proper use of chemicals and substances is upheld.</p></div>
            <div><img class='icon-logo' src="img\certified.png" alt="chemicals">
            <p id='chem1-text'>To ensure compliance, our products are <br><span class="highlight">regularly checked</span> by third-party <span class="highlight">certified testing laboratories.</span></p></div>
        </div>
    </div>
</div> <!-- end infopack -->