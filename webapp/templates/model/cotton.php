<div id='infopack'>
    <div class="slideContainer">
        <div class="slider">

        <div><img class='anim-img' src="gif\cottonani1.gif" alt="cotton">
        <p>By 2025 we plan to reach <span class="highlight">100%</span>! One clothing piece will use a 
            maximum of <span class="highlight">20%</span> recycled and <span class="highlight">80%</span> organic cotton for ultimate durability.</p></div>
            <div><img class='icon-logo' src="img\nogmo.png" alt="cotton">
                <p> We work on replacing all conventional cotton in our collections 
                with <span class="highlight">organic or recycled cotton fibers</span>. For cotton to be considered organic, 
                it must be grown on land free from pesticides and fertilizers for at least 
                three years, <span class="highlight">without using GMOs.</span></p></div>
            <div><img class='bci-logo' src="img\bci-logo@2x.png" alt="cotton">
            <p>We support the Better Cotton Initiative (BCI) as a better solution where technical 
                or supply chain limitations are not fully in place to source <span class="highlight">organic or recycled fibers.</span>
            </p></div>
        </div>
    </div> <!-- end slideContainer -->
</div> <!-- end infopack -->