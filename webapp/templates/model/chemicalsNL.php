<div id='infopack'>
    <div class="slideContainer">
        <div class="slider">
            <div><img class='anim-img' src="gif\chem-transparent.gif" alt="chemicals">
            <p id='chem1-text'> We hebben een lijst met meer dan <span class="highlight">100 chemicaliën</span> (inclusief chemische afwerkingen en procedures) 
                die we <span class="highlight">niet gebruiken</span> bij het maken van onze collecties.</p></div>
            <div><img class='icon-logo' src="img\checklist.png" alt="chemicals">
            <p id='chem2-text'> <span class="highlight">Al onze leveranciers</span> verplichten zich tot en voldoen aan de lijst, die ervoor zorgt dat de wetgeving rondom het
             correct gebruik van chemicaliën en stoffen wordt nageleefd.</p></div>
            <div><img class='icon-logo' src="img\certified.png" alt="chemicals">
            <p id='chem1-text'>Om het naleving van deze regels te garanderen, worden onze producten <br><span class="highlight">regelmatig gecontroleerd</span> door <span class="highlight">externe gecertificeerde testlaboratoria.</span></p></div>
        </div>
    </div>
</div> <!-- end infopack -->