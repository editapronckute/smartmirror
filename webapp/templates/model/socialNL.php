<div id='infopack'>

    <div class="slideContainer">
        <div class="slider">
            
            <div><img class='icon-logo' src="img\coc@2x.png" alt="Code-of-Conduct"><p>Onze gedragscode is van <span class="highlight">fundamenteel belang</span> in de partnerschappen met onze leveranciers.</p></div>
            <div><img class='icon-logo' src="img\values@2x.png" alt="Value & Vurtues"><p>Hoewel we ons realiseren dat culturen over de hele wereld verschillende normen en waarden hebben, zijn sommige universeel geldig en van toepassing op <span class="highlight">al onze activiteiten.</span></p></div>
            <div><img class='icon-logo' src="img\culture@2x.png" alt="culture"><p>We werken samen met leveranciers die <span class="highlight">onze gedragscode delen</span>, binnen de context van hun specifieke cultuur en die deze ook delen met hun leveranciers.</p></div>
            <div><img class='icon-logo' src="img\noRacism@2x.png" alt="No Racism"><p>Wij staan voor een <span class="highlight">eerlijke werkomgeving</span>, geen enkele vorm van discriminatie en nog veel meer.</p></div>
        </div>
    </div>
</div> <!-- end infopack -->