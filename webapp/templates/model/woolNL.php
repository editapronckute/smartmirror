<div id='infopack'>
    <div class="slideContainer">
        <div class="slider">
            <div><img class='anim-img' src="gif\sheep-transparent.gif" alt="wool">
            <p>Ongeveer 800.000 schapen moeten minstens één keer per jaar geschoren worden. Elk schaap levert <br><span class="highlight">4 kg ruwe wol</span>, waarmee twee truien kunnen worden gebreid.
            </p></div>
            <div><img class='woolstats-img' src="img\woolstats.png" alt="wool"></div>
        </div>
    </div>
</div> <!-- end infopack -->