<div id='infopack'>

    <div class="slideContainer">
    <div class="slider">
        <div><img class='anim-img' src="gif\pack-transparent.gif" alt="packaging">
        <p>Onze tassen zijn gemaakt van <span class="highlight">20% biobased plastic</span>, dat in drie tot zes maanden volledig uiteenvalt. Het kan thuis worden gecomposteerd.</p></div>
        <div><img class='icon-logo' src="img\open-box.png" alt="packaging"><p>We gebruiken FSC-gecertificeerd papier en karton voor al onze hangtags en verzenddozen. 
            Alle nieuwe winkels zijn uitgerust met hangers gemaakt van <br>RCS-gecertificeerd Fasal®, een houten compound gemaakt van <span class="highlight">hernieuwbare bronnen</span> en ontworpen om 
            circulair te zijn.</p></div>
        <div><img class='icon-logo' src="img\thread.png" alt="packaging"><p>Voor textiel gebruiken we <span class="highlight">gerecycled</span> nylon, polyester, katoen en leer, gemaakt van afval van de textielindustrie, ongewenste kleding en andere bestaande bronnen.</p></div>
    </div>
    </div>
</div> <!-- end infopack -->