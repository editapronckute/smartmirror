<div id='infopack'>

    <div class="slideContainer">
        <div class="slider">
            <div><img class='anim-img' src="gif\denim-transparent.gif" alt="water">
        <p>The e-Flow® technology reduces water consumption during the finishing process, <br> 
        <span class="highlight">saving 70-80%</span> of water.</p>
        </div>
            <div><img class='anim-img' src="gif\water-transparent.gif" alt="water">
            <p> <br> LENZING™ and ECOVERO™ fibre generates up to <span class="highlight">50% lower water impact</span> and emissions compare to conventional viscose.</p></div>
            <div><img class='bci-logo' src="img\bci-logo@2x.png" alt="water">
            <p> Better Cotton Initiative (BCI) that we support also is requiring farmers to <span class="highlight">use water efficiently.</span></p></div>
        </div>
    </div>
</div> <!-- end infopack -->