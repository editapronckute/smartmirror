<div id='infopack'>

    <div class="slideContainer">
    <div class="slider">
        <div><img class='anim-img' src="gif\pack-transparent.gif" alt="packaging">
        <p>Our bags are made from <span class="highlight">20% bio-based plastic</span>, that fully disintegrate in three to six months.
            It can be composted at home.</p></div>
        <div><img class='icon-logo' src="img\open-box.png" alt="packaging"><p>We use FSC certified paper and cardboard for all our hang tags & shipping boxes.
        All new stores are fitted with hangers made from RCS certified Fasal®, a wooden 
        compound made from <span class="highlight">renewable resources</span> and designed to be circular.</p></div>
        <div><img class='icon-logo' src="img\thread.png" alt="packaging"><p>For textiles we use <span class="highlight">recycled</span> nylon, polyester, cotton and leather, made of made 
        from textile industry waste, unwanted garments and other existing resources.</p></div>
    </div>
    </div>
</div> <!-- end infopack -->