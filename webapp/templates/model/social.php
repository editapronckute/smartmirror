<div id='infopack'>

    <div class="slideContainer">
        <div class="slider">   
            <div><img class='icon-logo' src="img\coc@2x.png" alt="Code-of-Conduct"><p>Our Code of Conduct is <span class="highlight">fundamental</span> in the partnerships 
            with our suppliers.</p></div>
            <div><img class='icon-logo' src="img\values@2x.png" alt="Value & Vurtues"><p>Although we realize that cultures across the globe 
            have different norms and values, certain virtues are universally valid and apply 
            to <span class="highlight">all our activities.</span></p></div>
            <div><img class='icon-logo' src="img\culture@2x.png" alt="culture"><p>We work with suppliers that <span class="highlight">share our Code of Conduct</span>, 
            within the context of their particular culture and who share these also with 
            their suppliers.</p></div>
            <div><img class='icon-logo' src="img\noRacism@2x.png" alt="No Racism"><p>We stand for <span class="highlight">fair working environment</span>, no discrimination of 
            any kind and much more.</p></div>
        </div>
    </div>
</div> <!-- end infopack -->