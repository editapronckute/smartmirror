<div id='infopack'>

    <div class="slideContainer">
        <div class="slider">
            <div><img class='anim-img' src="gif\denim-transparent.gif" alt="water">
        <p>De e-Flow®-technologie vermindert het waterverbruik tijdens het afwerkingsproces, <br>
        waardoor <span class="highlight">70-80% water wordt bespaard.</span> </p>
        </div>
            <div><img class='anim-img' src="gif\water-transparent.gif" alt="water">
            <p> <br> LENZING™- en ECOVERO™-vezels genereren tot <span class="highlight">50% minder water impact</span> en uitstoot in vergelijking met conventionele viscose.</p></div>
            <div><img class='bci-logo' src="img\bci-logo@2x.png" alt="water">
            <p> Het Better Cotton Initiative (BCI) dat we ondersteunen, vereist ook van boeren dat ze <span class="highlight">efficiënt met water omgaan.</span></p></div>
        </div>
    </div>
</div> <!-- end infopack -->