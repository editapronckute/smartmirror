<div id='wrapper'>
    <div id=infoPanel> <?php include 'templates/model/directions.php'; ?></div>
    <a href='templates/model/cotton.php' class='linktxt elipses' id='cottonlink'> 
        <img src="img/cotton@2x.png" class='iconimg'>
        <div id='cotton'><p class='iname' id='cotton-Lang-switch'>Cotton</p></div>
    </a> <!-- end cotton -->
    <a href='templates/model/water.php' class='linktxt elipses' id='waterlink'> 
        <img src="img/water@2x.png" class='iconimg'> 
        <div id='water'><p class='iname' id='water-Lang-switch'>Water</p></div>
    </a> <!-- end water -->
    <a href='templates/model/wool.php' class='linktxt elipses' id='woollink'>
        <img src="img/wool@2x.png" class='iconimg'>  
        <div id='wool'><p class='iname' id='wool-Lang-switch'>Wool</p></div>
    </a> <!-- end wool -->
    <a href='templates/model/social.php' class='linktxt elipses' id='sociallink'>
        <img src="img/social@2x.png" class='iconimg'>
        <div id='social'><p class='iname' id='social-Lang-switch'>Social Responsibility</p></div>
    </a> <!-- end social -->
    <a href='templates/model/packaging.php' class='linktxt elipses' id='packagelink'>
        <img src="img/packaging@4x.png" class='iconimg'>
        <div id='packaging'><p class='iname'id='packaging-Lang-switch'>Packaging</p></div>
    </a> <!-- end packaging -->
    <a href='templates/model/chemicals.php' class='linktxt elipses' id='chemicalslink'>
        <img src="img/chemicals@2x.png" class='iconimg'>
        <div id='chemicals'><p class='iname' id='chems-Lang-switch'>Chemicals</p></div>
    </a> <!-- end chemicals -->
</div>

<div id="langcontainer">
    
        <div id="en" class="lang-btn"><p class="language">ENGLISH</p> </div>
        <div id="nl" class="lang-btn"><p class="language">DUTCH</p> </div>
        <div id='btnslider'></div>
</div>


