<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MagicMirror</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    

    <meta name="description" content="Smart Mirror"/>
    <meta name="theme-color" content="#4dbaa8"/>



   
    <!-- <link rel="icon" href="img/logo-web@2x-01.png"> -->
</head>