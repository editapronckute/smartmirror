from algosdk.transaction import *
from algosdk.mnemonic import *
from algosdk.account import *
from algosdk.algod import AlgodClient
import RPi.GPIO as GPIO
import SimpleMFRC522
from time import sleep
import subprocess
import webbrowser
import base64
import hashlib

test_algodnet = "http://18.217.181.193:10101"
test_algodtoken = "600c73753593b6cba37dc7061105177a7aa774fe31ffd77de76fbc3430d72548"
client = AlgodClient(test_algodtoken, test_algodnet)
address = "FLEYPHNBUTDSCBSEJ2NDDB3NH3F7SWWEDF46CNBMROATQSA3XONHDQRLKY"

def slow_type(message):
    for character in message:
        print(character, end="")
        sleep(0.01)
    print("\n")

reader = SimpleMFRC522.SimpleMFRC522()

slow_type("Electromaker RFID User Interface V1.0\n********************\nHold a tag near the reader to start an application\n")

try:
    while True:
        id, text = reader.read()
        #print(id)
        sleep(0.1)     
        if id == 651502695298:
            print("Account: {}\nPending Rewards: {}".format(address, client.account_info(address).get('pendingrewards')))
        elif id == 720298062653:
            print("Account: {}\nAmount: {}".format(address, client.account_info(address).get('amount')))
        elif id == 693927025770:
            print("Account: {}\nTotal Rewards: {}".format(address, client.account_info(address).get('rewards')))
except KeyboardInterrupt:
    slow_type("Exiting the application, bye!")
    GPIO.cleanup()