import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522

sustainable = [731778131087, 767246539896, 629489802394]

reader = SimpleMFRC522()

try: 
    id, text = reader.read()
    if id in sustainable :
        print("Item is sustainable! : ", id)
    else :
        print("Item is not sustainable, you should reconsider your choices")

finally:
    GPIO.cleanup()